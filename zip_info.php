<!DOCTYPE html>
<html>
<head>
<style>
table {
    width: 100%;
    border-collapse: collapse;
}

table, td, th {
    border: 1px solid black;
    padding: 5px;
}

th {text-align: left;}
</style>
</head>
<body>
<?php
include_once('zipconnection.php');
$zip = $_POST['zip'];
$sqlget="SELECT zip,city FROM zipcity WHERE zip=$_POST[zip]";
$res=mysqli_query($conn, $sqlget);
echo "<table>
<tr>
<th>Zip</th>
<th>City</th>
</tr>";
while($row = mysqli_fetch_array($res)) {
    echo "<tr>";
    echo "<td>" . $row['zip'] . "</td>";
    echo "<td>" . $row['city'] . "</td>";
    echo "</tr>";
}
echo "</table>";
mysqli_close($conn);
?>
</body>
</html>